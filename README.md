### General information ###

* Developed on atom
* Arduino RF remote temperature humidity probe
* LiFePO4 battery powered
* RFM69 radio module
* DHT11 temp probe
* Modified Arduino Pro Mini as UC 
* arduino deep sleep
* custom temp update intervals

### Arduino Pro Mini low power modifications ###

* disable LEDs
* [Low Power Arduino](http://home-automation-community.com/arduino-low-power-how-to-run-atmega328p-for-a-year-on-coin-cell-battery)
* Powered by 3.2v LiFePO4 battery (700 mAh)

#### Measured current consumption for the sketch
- deep sleep current => 4.4 uA without oled and 11uA
- current when status displayed on oled => 12.16 mA for approx. 4 sec.
- current when sending data => 20mA for 250ms
- a 700mAh 3.2v LiFePO4 battery would last more than 1y when sending data 4 times
per hour and showing the status once per hour.

### Users manual ###
* For status display, hold button down for a brief period of uptime
* To change Temp/humidity update interval, hold button down for over 1s

### Image copyrights ###
Icons are provided by SimpleIcon (https://www.flaticon.com/authors/simpleicon)
