// Sends perdiodic temperature/humidity measurements to a base station
// The gateway ID is always 1
// Uses a DHT11 temperature/humidity probe
// **********************************************************************************
#include <RFM69.h>         //get it here: https://www.github.com/lowpowerlab/rfm69
#include <RFM69_ATC.h>     //get it here: https://www.github.com/lowpowerlab/rfm69
#include <SPI.h>           //included with Arduino IDE install (www.arduino.cc)
#include "DHT.h"
#include <LowPower.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include "images.h"
#include <limits.h>

//*********************************************************************************************
//************ IMPORTANT SETTINGS - YOU MUST CHANGE/CONFIGURE TO FIT YOUR HARDWARE ************
//*********************************************************************************************
#define NODEID        2    //must be unique for each node on same network (range up to 254, 255 is used for broadcast)
#define NETWORKID     100  //the same on all nodes that talk to each other (range up to 255)
#define GATEWAYID     1
#define FREQUENCY     RF69_868MHZ
#define ENCRYPTKEY    "0123456789012345" //exactly the same 16 characters/bytes on all nodes!
//#define IS_RFM69HW_HCW  //uncomment only for RFM69HW/HCW! Leave out if you have RFM69W/CW!
#define ENABLE_ATC    //comment out this line to disable AUTO TRANSMISSION CONTROL
#define ATC_RSSI      -80
//*********************************************************************************************
#define OLED_RESET 4
#define BATT_DISCHARGE 3000
#define DHTTYPE       DHT11
#define DHT_PIN       8  // DHT11 Signal PIN
#define DHT_VCC       7  // Pin used to power the DHT sensor
#define NUM_SAMPLES   10 // Voltage sample count
#define BUTTON_PIN    3  // Button input pin for generating interrupt.
                         // Pin 2 is used by RFM69
#define SERIAL_BAUD   115200
#define SERIAL_EN
#define DISPLAY_EN
#define SCREEN_ON_TIME 4000 // Time to keep oled on

//*********************************************************************************************
#ifdef SERIAL_EN
  #define DEBUG(input)   {Serial.print(input); delay(1);}
  #define DEBUGln(input) {Serial.println(input); delay(1);}
#else
  #define DEBUG(input);
  #define DEBUGln(input);
#endif
//*********************************************************************************************
#ifdef DISPLAY_EN
  Adafruit_SSD1306 display(OLED_RESET);
#endif
int TR_PERIODS[5] = {30, 60, 300, 900, 1800};
short currPeriod = 2;
int TRANSMITPERIOD = TR_PERIODS[currPeriod]; // transmit a packet to gateway every X seconds
float t = 0.0;
float h = 0.0;
long lastPeriod = 0;
int sleep_cnt = 500;

// The datastructure used for communication with the gateway
typedef struct {
  uint32_t battVoltage;
  char data[10];
} data_t;

data_t theData;
DHT dht(DHT_PIN, DHTTYPE);

volatile short buttonState = HIGH;
unsigned long lastDebounceTime = ULONG_MAX;
unsigned long screenOnStartTs = 0;
bool isScreenOn = false;
bool keepAlive = false;

#ifdef ENABLE_ATC
  RFM69_ATC radio;
#else
  RFM69 radio;
#endif

// Function used for disabling PINs in order to reduce power consumption
void downPins() {
  for (uint8_t i=0; i<=A5; i++)
  {
    if (i == RF69_SPI_CS) continue;
    if (i == A0) continue;
    if (i == BUTTON_PIN) continue;
    pinMode(i, OUTPUT);
    digitalWrite(i, LOW);
  }
}

// Reads the VCC voltage and returns the value in millivolts
long readVcc() {
  // Read 1.1V reference against AVcc
  // set the reference to Vcc and the measurement to the internal 1.1V reference
  #if defined(__AVR_ATmega32U4__) || defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
    ADMUX = _BV(REFS0) | _BV(MUX4) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  #elif defined (__AVR_ATtiny24__) || defined(__AVR_ATtiny44__) || defined(__AVR_ATtiny84__)
    ADMUX = _BV(MUX5) | _BV(MUX0);
  #elif defined (__AVR_ATtiny25__) || defined(__AVR_ATtiny45__) || defined(__AVR_ATtiny85__)
    ADMUX = _BV(MUX3) | _BV(MUX2);
  #else
    ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  #endif

  delay(2); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Start conversion
  while (bit_is_set(ADCSRA,ADSC)); // measuring

  uint8_t low  = ADCL; // must read ADCL first - it then locks ADCH
  uint8_t high = ADCH; // unlocks both

  long result = (high<<8) | low;

  result = 1125300L / result; // Calculate Vcc (in mV); 1125300 = 1.1*1023*1000
  return result; // Vcc in millivolts
}

void oled_on() {
  buttonState = digitalRead(BUTTON_PIN);
}

void readTemp() {
  digitalWrite(DHT_VCC, HIGH);
  dht.begin();
  t = dht.readTemperature();
  h = dht.readHumidity();
  DEBUG("Temp: "); DEBUGln(t);
  DEBUG("Hum: "); DEBUGln(h);
  digitalWrite(DHT_VCC, LOW);
  downPins();
}
void showTempIcn() {
  #ifdef DISPLAY_EN
    display.ssd1306_command(SSD1306_DISPLAYON);
    display.clearDisplay();
    display.dim(true);
    display.drawBitmap(48, 0, temp_icon, temp_icon_width, temp_icon_height, WHITE);
    display.display();
    delay(1000);
    display.clearDisplay();
    display.ssd1306_command(SSD1306_DISPLAYOFF);
  #endif
}

void setup() {
  Serial.begin(SERIAL_BAUD);
  attachInterrupt(digitalPinToInterrupt(BUTTON_PIN), oled_on, CHANGE);
  #ifdef DISPLAY_EN
    display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
    showTempIcn();
  #endif

  keepAlive = false;
  // Setting up and powering off DHT sensor
  pinMode(DHT_VCC, OUTPUT);
  digitalWrite(DHT_VCC, LOW);

  // Setting button pin as input
  pinMode(BUTTON_PIN, INPUT);


  radio.initialize(FREQUENCY,NODEID,NETWORKID);
  #ifdef IS_RFM69HW_HCW
    radio.setHighPower(); //must include this only for RFM69HW/HCW!
  #endif
  radio.encrypt(ENCRYPTKEY);

  //Auto Transmission Control - dials down transmit power to save battery (-100 is the noise floor, -90 is still pretty good)
  //For indoor nodes that are pretty static and at pretty stable temperatures (like a MotionMote) -90dBm is quite safe
  //For more variable nodes that can expect to move or experience larger temp drifts a lower margin like -70 to -80 would probably be better
  //Always test your ATC mote in the edge cases in your own environment to ensure ATC will perform as you expect
  #ifdef ENABLE_ATC
    radio.enableAutoPower(ATC_RSSI);
  #endif

  #ifdef ENABLE_ATC
    DEBUGln("RFM69_ATC Enabled (Auto Transmission Control)\n");
  #endif
  radio.sleep();
  Serial.flush();
}

void showBattLvl() {
  int dvcc = readVcc() - BATT_DISCHARGE;
  #ifdef DISPLAY_EN
    display.ssd1306_command(SSD1306_DISPLAYON);
    display.clearDisplay();
    display.dim(true);
    if(dvcc > 100) {
      display.drawBitmap(0, 8,  battery_100, batt_width, batt_height, 1);
    } else if(dvcc > 40) {
      display.drawBitmap(0, 8,  battery_60, batt_width, batt_height, 1);
    } else if(dvcc > 10) {
      display.drawBitmap(0, 8,  battery_20, batt_width, batt_height, 1);
    } else {
      display.drawBitmap(0, 8,  battery_20, batt_width, batt_height, 1);
    }
    display.display();
  #endif
}

void displayTemp() {
  readTemp();
  short temp = (int)t;
  short hum = (int)h;
  DEBUG("temp = "); DEBUGln(temp);
  if(temp > 0 and hum > 0) {
    showBattLvl();
    display.setCursor(20,4);
    display.setTextSize(3);
    display.setTextColor(WHITE);
    display.print(temp);
    display.setTextSize(1); display.print((char)247); display.print("C");
    display.display();
    display.drawLine(70, 4, 70, 28, WHITE);
    display.setCursor(75,4);
    display.setTextSize(3);
    display.print(hum);
    display.setTextSize(1); display.print("%");
    display.display();
    screenOnStartTs = millis();
    isScreenOn = true;
  }
}

void changeTransmitTime() {
  short len = sizeof(TR_PERIODS) / sizeof(TR_PERIODS[0]);
  short newPeriod = (currPeriod + 1) % len;
  TRANSMITPERIOD = TR_PERIODS[newPeriod];
  currPeriod = newPeriod;
  sleep_cnt = 0;
  showBattLvl();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(30,8);
  display.println("New interval");
  display.setCursor(50,19);
  if((TRANSMITPERIOD / 60) == 0) {
    display.print(TRANSMITPERIOD); display.print(" sec.");
  } else {
    display.print(TRANSMITPERIOD / 60); display.print(" min.");
  }
  display.display();
  screenOnStartTs = millis();
  isScreenOn = true;
}

void screenOff() {
  if((millis() - screenOnStartTs) >= SCREEN_ON_TIME) {
    if(buttonState == HIGH) {
      keepAlive = false;
      display.clearDisplay();
      display.ssd1306_command(SSD1306_DISPLAYOFF);
      isScreenOn = false;
    }
  }
}

void processButton() {

  #ifdef DISPLAY_EN
    unsigned int delta = 0;
    if(buttonState == LOW && (keepAlive == false || isScreenOn)) {
      keepAlive = true;
      if(lastDebounceTime == ULONG_MAX)
        lastDebounceTime = millis();
    }
    if(buttonState == HIGH && keepAlive == true){
      if(lastDebounceTime != ULONG_MAX) {
        delta = millis() - lastDebounceTime;
        //DEBUG("Button down time: "); DEBUGln(delta);
      }
      if(delta >= 1000) {
        Serial.println("Change transmit");
        lastDebounceTime = ULONG_MAX;
        changeTransmitTime();
      }else if(delta >= 100) {
        Serial.println("Display temp");
        lastDebounceTime = ULONG_MAX;
        displayTemp();
      }
    }
    if(isScreenOn)
      screenOff();
  #endif
}
/*
**** MAIN ****
*/

void loop() {
  processButton();

  //check for any received packets
  long diff = (millis() - lastPeriod) / 1000 + sleep_cnt * 8;

  if (diff >= TRANSMITPERIOD)
  {
    sleep_cnt = 0;
    lastPeriod=millis();
    readTemp();

    theData.battVoltage = readVcc();
    String temp = String(t, 0) + "," + String(h, 0);
    temp.toCharArray(theData.data, sizeof(theData.data));
    DEBUGln("Sending data");
    if(!isScreenOn) {
      showBattLvl();
      display.drawBitmap(48, 0, transmit, transmit_icon_width, transmit_icon_height, WHITE);
      display.display();
      delay(100);
    }
    radio.receiveDone();
    radio.send(GATEWAYID, (const void*)(&theData), sizeof(theData));
    if(!isScreenOn) {
      display.clearDisplay();
      display.ssd1306_command(SSD1306_DISPLAYOFF);
    }
    DEBUGln("Data sent");
    downPins();
  }

  Serial.flush();
  if(keepAlive == false) {
    DEBUGln("Putting everything to sleep");
    DEBUGln();
    radio.sleep();
    Serial.flush();
    radio.sleep();
    downPins();
    LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
    sleep_cnt++;
  }

}
