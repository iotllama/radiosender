EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:rfm69
LIBS:mysensors_arduino
LIBS:mysensors_connectors
LIBS:mysensors_logic
LIBS:mysensors_mcu
LIBS:mysensors_memories
LIBS:mysensors_network
LIBS:mysensors_radios
LIBS:mysensors_regulators
LIBS:mysensors_security
LIBS:mysensors_sensors
LIBS:SSD1306_OLED-0.91-128x32
LIBS:74xgxx
LIBS:ac-dc
LIBS:actel
LIBS:allegro
LIBS:Altera
LIBS:analog_devices
LIBS:battery_management
LIBS:bbd
LIBS:bosch
LIBS:brooktre
LIBS:cmos_ieee
LIBS:dc-dc
LIBS:diode
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS:ftdi
LIBS:gennum
LIBS:graphic_symbols
LIBS:hc11
LIBS:infineon
LIBS:intersil
LIBS:ir
LIBS:Lattice
LIBS:leds
LIBS:LEM
LIBS:logic_programmable
LIBS:maxim
LIBS:mechanical
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic24mcu
LIBS:microchip_pic32mcu
LIBS:modules
LIBS:motor_drivers
LIBS:msp430
LIBS:nordicsemi
LIBS:nxp
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:Oscillators
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:references
LIBS:rfcom
LIBS:RFSolutions
LIBS:sensors
LIBS:silabs
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:transf
LIBS:triac_thyristor
LIBS:ttl_ieee
LIBS:video
LIBS:wiznet
LIBS:Worldsemi
LIBS:Xicor
LIBS:zetex
LIBS:Zilog
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Arduino RFM69 Board"
Date "2018-01-28"
Rev "1.0.0"
Comp "IOT llama"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ArduinoProMini IC1
U 1 1 5A6E2439
P 3150 3900
F 0 "IC1" H 2400 5150 40  0000 L BNN
F 1 "ArduinoProMini 16Mhz" H 3550 2500 40  0000 L BNN
F 2 "mysensors_arduino:pro_mini_cosmin" H 3150 3900 30  0001 C CIN
F 3 "" H 3150 3900 60  0000 C CNN
	1    3150 3900
	1    0    0    -1  
$EndComp
$Comp
L oled_091 U2
U 1 1 5A6E2BA1
P 7300 4300
F 0 "U2" H 8100 4650 60  0000 R TNN
F 1 "oled_091" H 7300 4650 60  0000 L TNN
F 2 "Pin_Headers:Pin_Header_Angled_1x04_Pitch2.54mm" H 7600 4250 60  0001 C CNN
F 3 "" H 7600 4250 60  0001 C CNN
	1    7300 4300
	-1   0    0    -1  
$EndComp
$Comp
L SW_Push SW1
U 1 1 5A6E2C44
P 4750 3300
F 0 "SW1" V 4700 3400 50  0000 L CNN
F 1 "SW_Push" H 4750 3240 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH-12mm" H 4750 3500 50  0001 C CNN
F 3 "" H 4750 3500 50  0001 C CNN
	1    4750 3300
	0    -1   1    0   
$EndComp
$Comp
L R R1
U 1 1 5A6E2CD5
P 5200 3100
F 0 "R1" V 5280 3100 50  0000 C CNN
F 1 "10k" V 5200 3100 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 5130 3100 50  0001 C CNN
F 3 "" H 5200 3100 50  0001 C CNN
	1    5200 3100
	0    1    1    0   
$EndComp
$Comp
L RFM69HW U3
U 1 1 5A6E2E37
P 8500 4450
F 0 "U3" H 8150 4700 40  0000 C CNN
F 1 "RFM69HW" H 8750 3700 40  0000 C CNN
F 2 "mysensors_radios:RFM69HW_SMD_Handsoldering" H 8500 4450 30  0001 C CIN
F 3 "" H 8500 4450 60  0000 C CNN
	1    8500 4450
	1    0    0    -1  
$EndComp
$Comp
L DHT11 U1
U 1 1 5A6E2EEB
P 6850 5250
F 0 "U1" H 7000 5500 50  0000 C CNN
F 1 "DHT11" H 6950 5000 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 7000 5500 50  0001 C CNN
F 3 "" H 7000 5500 50  0001 C CNN
	1    6850 5250
	-1   0    0    -1  
$EndComp
Text Notes 7050 3400 0    60   ~ 0
Power
Text GLabel 6950 2950 2    60   Input ~ 0
VCC
Text GLabel 6700 3150 2    60   Input ~ 0
GND
Text GLabel 2250 2800 0    60   Input ~ 0
VCC
NoConn ~ 2250 3100
NoConn ~ 2250 4600
NoConn ~ 2250 4700
Text GLabel 2250 5000 0    60   Input ~ 0
GND
Text Label 4650 4800 2    60   ~ 0
SDA
Text Label 4650 4900 2    60   ~ 0
SCL
Text Label 6650 4350 3    60   ~ 0
SDA
Text Label 6800 4350 3    60   ~ 0
SCL
Text GLabel 6950 4350 3    60   Input ~ 0
VCC
Text GLabel 7100 4350 3    60   Input ~ 0
GND
Text GLabel 5000 3600 3    60   Input ~ 0
GND
Text GLabel 5450 3100 2    60   Input ~ 0
VCC
Text Label 4650 3100 2    60   ~ 0
INT1
Text GLabel 8500 4050 1    60   Input ~ 0
VCC
Text Label 4650 3000 2    60   ~ 0
INT0
Text Label 9300 4450 2    60   ~ 0
INT0
NoConn ~ 9050 4550
NoConn ~ 9050 4650
NoConn ~ 9050 4750
NoConn ~ 9050 4850
NoConn ~ 9050 4950
NoConn ~ 7950 5000
NoConn ~ 7950 5100
Text GLabel 8500 5300 3    60   Input ~ 0
GND
$Comp
L Antenna AE1
U 1 1 5A6E6149
P 7950 4100
F 0 "AE1" H 7875 4175 50  0000 R CNN
F 1 "Antenna" H 7875 4100 50  0000 R CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 7950 4100 50  0001 C CNN
F 3 "" H 7950 4100 50  0001 C CNN
	1    7950 4100
	1    0    0    -1  
$EndComp
Text Label 4650 4200 2    60   ~ 0
CLOCK
Text Label 4650 4100 2    60   ~ 0
MISO
Text Label 4650 4000 2    60   ~ 0
MOSI
Text Label 4650 3800 2    60   ~ 0
NSS
Text Label 7600 4500 0    60   ~ 0
NSS
Text Label 7600 4600 0    60   ~ 0
MOSI
Text Label 7600 4700 0    60   ~ 0
MISO
Text Label 7600 4800 0    60   ~ 0
CLOCK
Text Label 4400 4800 2    60   ~ 0
A4
Text Label 4400 4900 2    60   ~ 0
A5
Text Label 4350 4200 2    60   ~ 0
D13
Text Label 4350 4100 2    60   ~ 0
D12
Text Label 4350 4000 2    60   ~ 0
D11
Text Label 4350 3800 2    60   ~ 0
D10
Text GLabel 6950 5550 3    60   Input ~ 0
GND
Text Label 6800 4950 0    60   ~ 0
D7
Text Label 6500 5400 1    60   ~ 0
D8
$Comp
L R R2
U 1 1 5A6E7259
P 6500 5100
F 0 "R2" V 6580 5100 50  0000 C CNN
F 1 "10k" V 6500 5100 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 6430 5100 50  0001 C CNN
F 3 "" H 6500 5100 50  0001 C CNN
	1    6500 5100
	-1   0    0    1   
$EndComp
Text Notes 4100 5650 2    60   ~ 0
Arduino Pro Mini
Text Label 4250 3100 0    60   ~ 0
D3
Text Label 4250 3000 0    60   ~ 0
D2
Text Notes 7700 5750 0    60   ~ 0
Peripherals
$Comp
L PWR_FLAG #FLG01
U 1 1 5A6EC7E8
P 6950 2800
F 0 "#FLG01" H 6950 2875 50  0001 C CNN
F 1 "PWR_FLAG" H 6950 2950 50  0000 C CNN
F 2 "" H 6950 2800 50  0001 C CNN
F 3 "" H 6950 2800 50  0001 C CNN
	1    6950 2800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 5A6ECDC1
P 6700 3200
F 0 "#PWR02" H 6700 2950 50  0001 C CNN
F 1 "GND" H 6700 3050 50  0000 C CNN
F 2 "" H 6700 3200 50  0001 C CNN
F 3 "" H 6700 3200 50  0001 C CNN
	1    6700 3200
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG03
U 1 1 5A6ED575
P 6700 3150
F 0 "#FLG03" H 6700 3225 50  0001 C CNN
F 1 "PWR_FLAG" V 6600 2900 50  0000 C CNN
F 2 "" H 6700 3150 50  0001 C CNN
F 3 "" H 6700 3150 50  0001 C CNN
	1    6700 3150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6700 2950 6950 2950
Wire Notes Line
	6200 2550 7400 2550
Wire Notes Line
	7400 2550 7400 3450
Wire Notes Line
	7400 3450 6200 3450
Wire Notes Line
	6200 3450 6200 2550
Wire Wire Line
	4150 4800 4650 4800
Wire Wire Line
	4150 4900 4650 4900
Wire Wire Line
	4150 3100 5050 3100
Wire Wire Line
	4750 3500 5000 3500
Wire Wire Line
	5000 3400 5000 3600
Wire Wire Line
	5350 3100 5450 3100
Wire Wire Line
	4150 3000 4750 3000
Wire Wire Line
	9050 4450 9300 4450
Wire Wire Line
	4150 4100 4650 4100
Wire Wire Line
	4150 4200 4650 4200
Wire Wire Line
	4150 4000 4650 4000
Wire Wire Line
	4150 3800 4650 3800
Wire Wire Line
	7950 4500 7600 4500
Wire Wire Line
	7950 4600 7600 4600
Wire Wire Line
	7950 4700 7600 4700
Wire Wire Line
	7950 4800 7600 4800
Wire Wire Line
	8400 5300 8600 5300
Wire Wire Line
	6950 4950 6950 4850
Wire Wire Line
	6500 5250 6550 5250
Wire Wire Line
	6500 4950 6950 4950
Connection ~ 6950 4950
Wire Wire Line
	6500 5250 6500 5400
Wire Notes Line
	1800 2500 5850 2500
Wire Notes Line
	5850 2500 5850 5850
Wire Notes Line
	5850 5850 1800 5850
Wire Notes Line
	1800 5850 1800 2500
Wire Notes Line
	6200 3750 9500 3750
Wire Notes Line
	9500 3750 9500 5850
Wire Notes Line
	9500 5850 6200 5850
Wire Notes Line
	6200 5850 6200 3750
Connection ~ 4750 3100
Wire Wire Line
	6700 3050 6700 3200
Wire Wire Line
	8500 4050 8500 4100
Connection ~ 2250 4900
Connection ~ 2250 5000
Connection ~ 7100 4350
Wire Wire Line
	6950 2950 6950 2800
Connection ~ 6950 2950
Connection ~ 6700 3150
Connection ~ -1300 5550
Connection ~ 5000 3500
Connection ~ 5000 3100
$Comp
L CP C1
U 1 1 5A6EE3E2
P 5000 3250
F 0 "C1" H 4900 3150 50  0000 L CNN
F 1 "1u" H 5025 3150 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D5.0mm_P2.00mm" H 5038 3100 50  0001 C CNN
F 3 "" H 5000 3250 50  0001 C CNN
	1    5000 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 4900 2250 4900
Wire Wire Line
	2250 4900 2250 5100
$Comp
L +3.3VA #PWR04
U 1 1 5A6F9245
P 6800 2950
F 0 "#PWR04" H 6800 2800 50  0001 C CNN
F 1 "+3.3VA" H 6800 2900 50  0000 C CNN
F 2 "" H 6800 2950 50  0001 C CNN
F 3 "" H 6800 2950 50  0001 C CNN
	1    6800 2950
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02 J6
U 1 1 5A6F947A
P 6500 3050
F 0 "J6" H 6500 3150 50  0000 C CNN
F 1 "Conn_01x02" H 6500 2850 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 6500 3050 50  0001 C CNN
F 3 "" H 6500 3050 50  0001 C CNN
	1    6500 3050
	-1   0    0    1   
$EndComp
Connection ~ 6800 2950
NoConn ~ 4150 2800
NoConn ~ 4150 2900
NoConn ~ 4150 3200
NoConn ~ 4150 3300
NoConn ~ 4150 3400
NoConn ~ 4150 3700
NoConn ~ 4150 4600
NoConn ~ 4150 4500
NoConn ~ 4150 4400
NoConn ~ 4150 5000
NoConn ~ 4150 5100
NoConn ~ 4150 4700
Text Label 4150 3500 0    60   ~ 0
D7
Text Label 4150 3600 0    60   ~ 0
D8
$EndSCHEMATC
